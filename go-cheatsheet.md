# go (golang) cheatsheet

![gopher](/sneak/docs/raw/branch/master/img/gopher.jpg)

## external

* awesome-go list of tools: [avelino/awesome-go](https://github.com/avelino/awesome-go)

## tools and environment

goimports manages your import lines and should be used instead of fmt, golangci-lint will tell you when you're doing silly things, and gotest will do the same thing as `go test` but with color output in terminal.

```
go get golang.org/x/tools/cmd/goimports
go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.31.0
go get -u github.com/rakyll/gotest
```

## code style

* obviously, always gofmt
* run golangci-lint

## types and interfaces

Accept and provide `String() string` method and the associated `fmt.Stringer` interface.

Accept and provide `GoString() string` method so `%#v` (see below) works right.

## testing

### use testify

```
import "github.com/stretchr/testify/assert"


func TestSomething(t *testing.T) {
	a := assert.New(t)

    thing, err := functionCall()
    
	t.Logf("%#v", thing)

	a.NotNil(err)
}
```


### use %#v

``` go
t.Logf("%#v", eim)
```

Outputs types with keys, viz:

```
    inner_test.go:17: &message.InnerMessage{
        ours:(*message.InnerJSONStruct)(nil),
        dstPubKey:(*cbk.CryptoBoxKey)(nil),
        srcPrivKey:(*cbk.CryptoBoxKey)(nil)}
```
