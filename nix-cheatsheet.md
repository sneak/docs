# nix cheatsheet

# my update command

`nix-env -iA nixpkgs.$(hostname -s)`

# external

* "learn nix in y minutes": https://learnxinyminutes.com/docs/nix/
